package nu.peg.poc.m326.protocol;

import java.io.Serializable;

/**
 * Marker interface for all Messages
 */
public interface Message extends Serializable{
}
