package nu.peg.poc.m326.protocol;

/**
 * joel @01.10.2016
 */
public class SimpleMessage implements Message {
    private String message;

    public SimpleMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return String.format("SimpleMessage{message='%s'}", message);
    }
}
