package nu.peg.poc.m326.network;

import nu.peg.poc.m326.network.internal.*;
import nu.peg.poc.m326.network.internal.strategy.ObjectSerializingMessageSerializationStrategy;
import nu.peg.poc.m326.protocol.Message;

/**
 * joel @01.10.2016
 */
public class NetworkFactory {
    public static ServerNetwork getDefaultServerNetwork(int port, ServerMessageHandler messageHandler) {
        StrategyServerNetwork<Message> network = new StrategyServerNetwork<>(port, messageHandler);
        network.setSerializationStrategy(new ObjectSerializingMessageSerializationStrategy());

        return network;
    }

    public static ClientNetwork getDefaultClientNetwork(String ip, int port, ClientMessageHandler messageHandler) {
        StrategyClientNetwork<Message> network = new StrategyClientNetwork<>(ip, port, messageHandler);
        network.setSerializationStrategy(new ObjectSerializingMessageSerializationStrategy());

        return network;
    }
}
