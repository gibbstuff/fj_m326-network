package nu.peg.poc.m326.network;

import nu.peg.poc.m326.protocol.Message;

/**
 * joel @01.10.2016
 */
public interface ServerNetwork {
    public void startListening();

    public void stop();

    public void send(String connectionId, Message message);

    public void broadcast(Message message);
}
