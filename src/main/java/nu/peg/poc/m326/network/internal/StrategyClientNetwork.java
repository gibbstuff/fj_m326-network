package nu.peg.poc.m326.network.internal;

import nu.peg.poc.m326.network.ClientMessageHandler;
import nu.peg.poc.m326.network.ClientNetwork;
import nu.peg.poc.m326.network.internal.strategy.MessageSerializationStrategy;
import nu.peg.poc.m326.protocol.Message;

import java.io.IOException;
import java.net.Socket;
import java.util.Deque;
import java.util.LinkedList;

/**
 * joel @01.10.2016
 */
public class StrategyClientNetwork<SerializedType> implements StrategyNetwork<SerializedType>, ClientNetwork, Runnable {
    private MessageSerializationStrategy<SerializedType> strategy;
    private ClientMessageHandler messageHandler;
    private Socket socket;
    private StreamHandler<SerializedType> streamHandler;
    private Thread meThread;
    private final Deque<Message> messageQueue;
    private Dispatcher dispatcher;
    private String ip;
    private int port;

    public StrategyClientNetwork(String ip, int port, ClientMessageHandler messageHandler) {
        this.messageHandler = messageHandler;
        this.ip = ip;
        this.port = port;
        this.messageQueue = new LinkedList<>();
        this.dispatcher = new Dispatcher();

        this.meThread = new Thread(this);
        this.meThread.setName(String.format("ClientNetwork %d ServerHandler", hashCode()));
    }

    @Override
    public void connect() {
        if (!meThread.isAlive()) {
            try {
                this.socket = new Socket(ip, port);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                this.streamHandler = strategy.provideStreamHandler(socket.getInputStream(), socket.getOutputStream());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            dispatcher.start();
            meThread.start();
        }
    }

    @Override
    public void disconnect() {
        meThread.interrupt();
        //noinspection deprecation
        meThread.stop();
        streamHandler.closeAllStreams();
        dispatcher.interrupt();
    }

    @Override
    public void setSerializationStrategy(MessageSerializationStrategy<SerializedType> strategy) {
        this.strategy = strategy;
    }

    @Override
    public MessageSerializationStrategy<SerializedType> getSerializationStrategy() {
        return strategy;
    }

    @Override
    public void send(Message message) {
        streamHandler.writeToStream(strategy.serialize(message));
    }

    @Override
    public void run() {
        while (!meThread.isInterrupted()) {
            SerializedType data = streamHandler.readFromStream();
            Message message = strategy.deserialize(data);

            synchronized (messageQueue) {
                messageQueue.add(message);
            }
        }
    }

    private class Dispatcher extends Thread {
        private Dispatcher() {
            this.setDaemon(true);
            this.setName(String.format("ClientNetwork %d Dispatcher", StrategyClientNetwork.this.hashCode()));
        }

        @Override
        public void run() {
            while (!interrupted()) {
                synchronized (messageQueue) {
                    Message message;
                    while ((message = messageQueue.poll()) != null) {
                        messageHandler.handle(message);
                    }
                }

                try {
                    Thread.sleep(10);
                } catch (InterruptedException ignored) {
                }
            }
        }
    }
}
