package nu.peg.poc.m326.network.internal.strategy;

import nu.peg.poc.m326.network.internal.StreamHandler;
import nu.peg.poc.m326.protocol.Message;

import java.io.*;

/**
 * joel @01.10.2016
 */
public class JsonMessageSerializationStrategy implements MessageSerializationStrategy<String> {
    @Override
    public String serialize(Message message) {
        return null;
    }

    @Override
    public Message deserialize(String serializedMessage) {
        return null;
    }

    @Override
    public StreamHandler<String> provideStreamHandler(InputStream is, OutputStream os) {
        return new StringBufferedStreamHandler(is, os);
    }

    private static class StringBufferedStreamHandler extends StreamHandler<String> {

        private final BufferedReader bufferedReader;
        private final BufferedWriter bufferedWriter;

        public StringBufferedStreamHandler(InputStream inputStream, OutputStream outputStream) {
            super(inputStream, outputStream);

            this.bufferedReader = new BufferedReader(new InputStreamReader(this.inputStream));
            this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(this.outputStream));
        }

        @Override
        public String readFromStream() {
            synchronized (bufferedReader) {
                try {
                    return bufferedReader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        public void writeToStream(String data) {
            synchronized (bufferedWriter) {
                try {
                    bufferedWriter.write(data, 0, data.length());
                    bufferedWriter.flush();
                } catch (IOException ie) {
                    ie.printStackTrace();
                }
            }
        }
    }
}
