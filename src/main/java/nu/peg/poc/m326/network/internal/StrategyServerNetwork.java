package nu.peg.poc.m326.network.internal;

import nu.peg.poc.m326.network.ServerMessageHandler;
import nu.peg.poc.m326.network.ServerNetwork;
import nu.peg.poc.m326.network.internal.strategy.MessageSerializationStrategy;
import nu.peg.poc.m326.protocol.Message;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

/**
 * joel @01.10.2016
 */
public class StrategyServerNetwork<SerializedType> implements ServerNetwork, StrategyNetwork<SerializedType>, Runnable {
    private final Thread meThread;
    private MessageSerializationStrategy<SerializedType> strategy;
    private ServerSocket serverSocket;
    private Map<String, ClientHandler> clients;
    private final Deque<ServerMessage> messageQueue;
    private ServerMessageHandler messageHandler;
    private Dispatcher dispatcher;
    private int port;

    public StrategyServerNetwork(int port, ServerMessageHandler messageHandler) {
        this.port = port;
        this.messageQueue = new LinkedList<>();
        this.messageHandler = messageHandler;
        this.clients = new HashMap<>();
        this.dispatcher = new Dispatcher();
        this.dispatcher.start();

        this.meThread = new Thread(this);
        meThread.setName(String.format("ServerNetwork %d Accepter", hashCode()));
    }

    @Override
    public void startListening() {
        try {
            this.serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (!meThread.isAlive())
            meThread.start();
    }

    @Override
    public void stop() {
        //noinspection deprecation
        meThread.interrupt();
        //noinspection deprecation
        clients.values().forEach(Thread::interrupt);
        clients.clear();

        dispatcher.interrupt();
    }

    @Override
    public void run() {
        while (!meThread.isInterrupted()) {
            try {
                Socket client = serverSocket.accept();
                System.err.printf("Accepted connection: %s%n", client);

                ClientHandler handler = new ClientHandler(client);
                clients.put(String.valueOf(handler.hashCode()), handler);
                handler.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void setSerializationStrategy(MessageSerializationStrategy<SerializedType> strategy) {
        this.strategy = strategy;
    }

    @Override
    public MessageSerializationStrategy<SerializedType> getSerializationStrategy() {
        return strategy;
    }

    @Override
    public void send(String connectionId, Message message) {
        clients.get(connectionId).send(message);
    }

    @Override
    public void broadcast(Message message) {
        for (ClientHandler client : clients.values()) client.send(message);
    }

    private static class ServerMessage {
        private final Message message;
        private final String connectionId;

        public ServerMessage(Message message, String connectionId) {
            this.message = message;
            this.connectionId = connectionId;
        }

        public Message getMessage() {
            return message;
        }

        public String getConnectionId() {
            return connectionId;
        }
    }

    private class ClientHandler extends Thread {
        private Socket socket;
        private StreamHandler<SerializedType> streamHandler;

        public ClientHandler(Socket socket) {
            this.socket = socket;
            try {
                this.streamHandler = strategy.provideStreamHandler(socket.getInputStream(), socket.getOutputStream());
            } catch (IOException ie) {
                throw new RuntimeException(ie);
            }

            this.setName(String.format("ServerNetwork %d ClientHandler %d", StrategyServerNetwork.this.hashCode(), this.hashCode()));
        }

        private void send(Message message) {
            streamHandler.writeToStream(strategy.serialize(message));
        }

        @Override
        public void run() {
            while (!interrupted()) {
                try {
                    SerializedType data = streamHandler.readFromStream();
                    Message message = strategy.deserialize(data);

                    synchronized (messageQueue) {
                        messageQueue.add(new ServerMessage(message, String.valueOf(this.hashCode())));
                    }
                } catch (Exception e) {
                    this.interrupt();
                    clients.remove(String.valueOf(hashCode()));
                }
            }
        }
    }

    private class Dispatcher extends Thread {
        private Dispatcher() {
            this.setDaemon(true);
            this.setName(String.format("ServerNetwork %d Dispatcher", StrategyServerNetwork.this.hashCode()));
        }

        @Override
        public void run() {
            while (!interrupted()) {
                synchronized (messageQueue) {
                    ServerMessage message;
                    while ((message = messageQueue.poll()) != null) {
                        messageHandler.handle(message.getMessage(), message.getConnectionId());
                    }
                }

                try {
                    // sleep to keep cpu usage on a maintainable level
                    Thread.sleep(10);
                } catch (InterruptedException ignored) {
                }
            }
        }
    }
}
