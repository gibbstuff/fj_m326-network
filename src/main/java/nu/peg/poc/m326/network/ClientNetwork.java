package nu.peg.poc.m326.network;

import nu.peg.poc.m326.protocol.Message;

/**
 * joel @01.10.2016
 */
public interface ClientNetwork {
    public void connect();

    public void disconnect();

    public void send(Message message);
}
