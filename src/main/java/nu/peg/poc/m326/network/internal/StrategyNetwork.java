package nu.peg.poc.m326.network.internal;

import nu.peg.poc.m326.network.internal.strategy.MessageSerializationStrategy;

/**
 * joel @01.10.2016
 */
public interface StrategyNetwork<SerializedType> {
    public void setSerializationStrategy(MessageSerializationStrategy<SerializedType> strategy);

    public MessageSerializationStrategy<SerializedType> getSerializationStrategy();
}
