package nu.peg.poc.m326.network.internal.strategy;

import nu.peg.poc.m326.network.internal.StreamHandler;
import nu.peg.poc.m326.protocol.Message;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * joel @01.10.2016
 */
public interface MessageSerializationStrategy<SerializedType> {
    public SerializedType serialize(Message message);

    public Message deserialize(SerializedType serializedMessage);

    public StreamHandler<SerializedType> provideStreamHandler(InputStream is, OutputStream os);
}
