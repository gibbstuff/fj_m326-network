package nu.peg.poc.m326.network;

import nu.peg.poc.m326.protocol.Message;

/**
 * joel @01.10.2016
 */
public interface ClientMessageHandler {
    public void handle(Message message);
}
