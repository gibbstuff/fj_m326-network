package nu.peg.poc.m326.network.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * joel @01.10.2016
 */
public abstract class StreamHandler<SerializedType> {
    protected InputStream inputStream;
    protected OutputStream outputStream;

    public StreamHandler(InputStream inputStream, OutputStream outputStream) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    public abstract SerializedType readFromStream();

    public abstract void writeToStream(SerializedType data);

    public void closeAllStreams() {
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
