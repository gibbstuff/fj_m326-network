package nu.peg.poc.m326.network.internal.strategy;

import nu.peg.poc.m326.network.internal.StreamHandler;
import nu.peg.poc.m326.protocol.Message;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 * joel @01.10.2016
 */
public class ObjectSerializingMessageSerializationStrategy implements MessageSerializationStrategy<Message> {
    @Override
    public Message serialize(Message message) {
        return message;
    }

    @Override
    public Message deserialize(Message serializedMessage) {
        return serializedMessage;
    }

    @Override
    public StreamHandler<Message> provideStreamHandler(InputStream is, OutputStream os) {
        return new ObjectSerializingStreamHandler(is, os);
    }

    private static class ObjectSerializingStreamHandler extends StreamHandler<Message> {
        private final ObjectInputStream objIs;
        private final ObjectOutputStream objOs;

        public ObjectSerializingStreamHandler(InputStream inputStream, OutputStream outputStream) {
            super(inputStream, outputStream);

            try {
                // Write the serialization header so that ObjectInputStream doesn't block
                objOs = new ObjectOutputStream(outputStream);
                objOs.flush();
                objIs = new ObjectInputStream(inputStream);
            } catch (IOException ie) {
                throw new RuntimeException(ie);
            }
        }

        @Override
        public Message readFromStream() {
            synchronized (objIs) {
                try {
                    return ((Message) objIs.readObject());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        public void writeToStream(Message data) {
            synchronized (objOs) {
                try {
                    objOs.writeObject(data);
                    objOs.flush();
                } catch (IOException ie) {
                    throw new RuntimeException(ie);
                }
            }
        }
    }
}
