package nu.peg.poc.m326;

import nu.peg.poc.m326.network.ClientNetwork;
import nu.peg.poc.m326.network.NetworkFactory;
import nu.peg.poc.m326.network.ServerNetwork;
import nu.peg.poc.m326.protocol.SimpleMessage;

/**
 * joel @01.10.2016
 */
public class App {
    public static void main(String[] args) throws InterruptedException {
        ServerNetwork serverNetwork = NetworkFactory.getDefaultServerNetwork(13377, ((message, connectionId) ->
                System.out.printf("Client \"%s\" sent: %s%n", connectionId, message)));

        ClientNetwork clientNetwork = NetworkFactory.getDefaultClientNetwork("127.0.0.1", 13377, (message -> System.out.printf("client1 received: %s%n", message)));
        ClientNetwork client2 = NetworkFactory.getDefaultClientNetwork("127.0.0.1", 13377, (message -> System.out.printf("client2 received: %s%n", message)));

        serverNetwork.startListening();
        clientNetwork.connect();

        for (int i = 0; i < 10; i++) {
            clientNetwork.send(new SimpleMessage("Hello!"));
        }
        serverNetwork.broadcast(new SimpleMessage("World!"));

        Thread.sleep(1000);

        client2.connect();
        serverNetwork.broadcast(new SimpleMessage("Twice!"));
        client2.send(new SimpleMessage("Some stuff"));

        clientNetwork.disconnect();
        client2.disconnect();
        serverNetwork.stop();
    }
}
